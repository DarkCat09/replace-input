# replace-input
Android accessibility service, modifies EditText fields lowercasing their text and replacing ё->е.  
[A post on 4PDA with this task](https://4pda.to/forum/index.php?act=findpost&pid=116735282) | [Main repository](https://codeberg.org/DarkCat09/replace-input) | [GitHub](https://github.com/DarkCat09/replace-input)